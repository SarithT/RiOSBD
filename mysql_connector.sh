#!/bin/bash
BASE_PATH=$(dirname $0)

echo "Waiting for mysql to get up"
# Give 100 seconds for master and slaves to come up
sleep 100

echo "Create MySQL Servers (master / slave repl)"
echo "-----------------"


echo "* Create replication user"

mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -AN -e "CREATE USER '$MYSQL_REPLICATION_USER'@'%' IDENTIFIED BY '$MYSQL_REPLICATION_PASSWORD';"
mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -AN -e "GRANT REPLICATION SLAVE ON *.* TO '$MYSQL_REPLICATION_USER'@'%';"
mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -AN -e 'flush privileges;'


echo "* Get MySQL01 log-file and it's position"

MYSQL01_Position=$(eval "mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G' | grep Position | sed -n -e 's/^.*: //p'")
echo $MYSQL01_Position
MYSQL01_File=$(eval "mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G'     | grep File     | sed -n -e 's/^.*: //p'")
echo $MYSQL01_File
MASTER_IP=$(eval "getent hosts mysqlmaster|awk '{print \$1}'")
echo $MASTER_IP

echo "* Set MySQL01 as master on MySQL02"

mysql --host mysqlslave -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "CHANGE MASTER TO master_host='$MASTER_IP', master_port=3306, \
        master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD', master_log_file='$MYSQL01_File', \
        master_log_pos=$MYSQL01_Position;"

echo "* Set MySQL01 as master on MySQL03"

mysql --host mysqlslave2 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "CHANGE MASTER TO master_host='$MASTER_IP', master_port=3306, \
        master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD', master_log_file='$MYSQL01_File', \
        master_log_pos=$MYSQL01_Position;"

#echo "* Set MySQL01 as master on MySQL04"
#
#mysql --host mysqlslave3 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "CHANGE MASTER TO master_host='$MASTER_IP', master_port=3306, \
#        master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD', master_log_file='$MYSQL01_File', \
#        master_log_pos=$MYSQL01_Position;"
#
#echo "* Set MySQL01 as master on MySQL05"
#
#mysql --host mysqlslave4 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "CHANGE MASTER TO master_host='$MASTER_IP', master_port=3306, \
#        master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD', master_log_file='$MYSQL01_File', \
#        master_log_pos=$MYSQL01_Position;"
#
#echo "* Set MySQL01 as master on MySQL06"
#
#mysql --host mysqlslave5 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "CHANGE MASTER TO master_host='$MASTER_IP', master_port=3306, \
#        master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD', master_log_file='$MYSQL01_File', \
#        master_log_pos=$MYSQL01_Position;"

echo "* Start Slave on both Servers"
mysql --host mysqlslave -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "start slave;"
mysql --host mysqlslave2 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "start slave;"
#mysql --host mysqlslave3 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "start slave;"
#mysql --host mysqlslave4 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "start slave;"
#mysql --host mysqlslave5 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "start slave;"


echo "Increase the max_connections to 2000"
mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -AN -e 'set GLOBAL max_connections=2000';
mysql --host mysqlslave -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'set GLOBAL max_connections=2000';
mysql --host mysqlslave2 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'set GLOBAL max_connections=2000';

mysql --host mysqlslave -uroot -p$MYSQL_MASTER_PASSWORD -e "show slave status \G"
mysql --host mysqlslave2 -uroot -p$MYSQL_MASTER_PASSWORD -e "show slave status \G"

SLAVE_IP=$(eval "getent hosts mysqlslave|awk '{print \$1}'")
SLAVE2_IP=$(eval "getent hosts mysqlslave2|awk '{print \$1}'")

echo "MySQL servers created!"
echo "--------------------"
echo
echo Variables available fo you :-
echo
echo MYSQL01_IP       : $MASTER_IP
echo MYSQL02_IP       : $SLAVE_IP
echo MYSQL03_IP       : $SLAVE2_IP
