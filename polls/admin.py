from django.contrib import admin

# Register your models here.
from polls.models import Employer, Hotel, Client, Service, ServicePeriod, ReservationPeriod


class EmployerAdmin(admin.ModelAdmin):
    list_display = ('name', 'lastName', 'hotel', 'telNumber', 'dateOfBirth')

admin.site.register(Employer, EmployerAdmin)

class HotelAdmin(admin.ModelAdmin):
    list_display = ('city', 'name', 'numberOfStars', 'numberOfRooms')

admin.site.register(Hotel, HotelAdmin)

class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'lastName', 'idNumber', 'dateOfBirth', 'telNumber')

admin.site.register(Client, ClientAdmin)

class ServiceAdmin(admin.ModelAdmin):
    list_display = ('serviceName', 'serviceDescription')

admin.site.register(Service, ServiceAdmin)


class ServicePeriodAdmin(admin.ModelAdmin):
    list_display = ('reservation', 'employer', 'serviceName', 'startDate', 'endDate', 'addInformation')

admin.site.register(ServicePeriod, ServicePeriodAdmin)

class ReservationPeriodAdmin(admin.ModelAdmin):
    list_display = ('client', 'hotel', 'startDate', 'endDate', 'roomNumber')

admin.site.register(ReservationPeriod, ReservationPeriodAdmin)
