# _*_ coding: utf-8 _*_
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import User
from django.forms import DateInput

from polls.models import Hotel, Employer, Client, Service, ServicePeriod, ReservationPeriod


class SignUpEmployerForm(UserCreationForm):
    name = forms.CharField(max_length=50, required=True, label="Imię")
    lastName = forms.CharField(max_length=50, required=True, label="Nazwisko")
    hotel = forms.ModelChoiceField(queryset=Hotel.objects.all(), required=True, label="Miejsce pracy")
    dateOfBirth = forms.DateField(required=True, label="Data urodzenia",
                                  widget=forms.DateInput(attrs={'placeholder': 'np. 1994-10-25'}))
    telNumber = forms.CharField(max_length=9, required=True, label="Numer telefonu")
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')
        labels = {'username': 'Nazwa użytkownika',
                  'email': 'email',
                  'password1': 'Hasło',
                  'password2': 'Powtórz hasło'}

    def save(self, commit=True):
        user = super(SignUpEmployerForm, self).save(commit=True)
        name = self.cleaned_data.get('name')
        lastName = self.cleaned_data.get('lastName')
        hotel = self.cleaned_data.get('hotel')
        dateOfBirth = self.cleaned_data.get('dateOfBirth')
        telNumber = self.cleaned_data.get('telNumber')
        employer_profile = Employer(name=name, lastName=lastName, hotel=hotel, dateOfBirth=dateOfBirth,
                                    telNumber=telNumber, user=user)
        if commit:
            employer_profile.save()
        return user, employer_profile

class ClientForm(forms.ModelForm):

    class Meta:
        model = Client
        exclude = ["staffMember"]
        fields = '__all__'
        labels = {'name': 'Imie',
                  'lastName': 'Nazwisko',
                  'dateOfBirth': 'Data urodzenia',
                  'idNumber': 'Numer dowodu osobistego',
                  'telNumber': 'Numer telefonu'}
        widgets = {
            'dateOfBirth': DateInput(attrs={'placeholder': 'np. 1994-10-25'})
        }

class ReservationPeriodForm(forms.ModelForm):

    class Meta:
        model = ReservationPeriod
        fields = '__all__'
        labels = {'client': 'Klient',
                  'hotel': 'Hotel',
                  'startDate': 'Data rozpoczęcia',
                  'endDate': 'Data zakończenia',
                  'roomNumber': 'Numer pokoju'}
        widgets = {
            'startDate': DateInput(attrs={'placeholder': 'np. 2017-10-25'}),
            'endDate': DateInput(attrs={'placeholder': 'np. 2017-10-25'})
        }


class ServicePeriodForm(forms.ModelForm):

    class Meta:
        model = ServicePeriod
        fields = '__all__'
        labels = {'reservation': 'Id rezerwacji',
                  'serviceName': 'Rodzaj usługi',
                  'employer': 'Pracownik odpowiedzialny za obsługę',
                  'startDate': 'Data rozpoczęcia',
                  'endDate': 'Data zakończenia',
                  'addInformation': 'Informacje dodatkowe'
                  }

        widgets = {
            'startDate': DateInput(attrs={'placeholder': 'np. 2017-10-25'}),
            'endDate': DateInput(attrs={'placeholder': 'np. 2017-11-02'})
        }