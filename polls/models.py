from django.core.validators import MaxValueValidator
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now

CITIES_TO_CHOOSE = (
        ('Polska', (
            ('Wrocław', 'Wrocław'),
            ('Warszawa', 'Warszawa'),
            ('Kraków', 'Kraków'),
        )
         ),
        ('Niemcy', (
            ('Berlin', 'Berlin'),
            ('Monachium', 'Monachium'),
        )
         ),
    )

def validate_startDate(date):
    if date < now().date():
        raise ValidationError(
            _('Podana data jest w przeszłości'),
            params={'date': date},
        )

def validate_endDate(endDate, startDate):
    if endDate < startDate:
        raise ValidationError(
            _('Data zakończenia musi być po dacie rozpoczęcia'),
        )

class Hotel(models.Model):
    city = models.CharField(max_length=45, choices=CITIES_TO_CHOOSE, db_column="miasto")
    name = models.CharField(max_length=45, db_column="nazwa")
    numberOfStars = models.PositiveIntegerField(default=4, validators=[MaxValueValidator(5)], db_column="liczba_gwiazdek")
    numberOfRooms = models.PositiveIntegerField(default=250, db_column = "liczba_pokoi")

    def hotelName(self):
        return self.name + " " + self.city

    def __unicode__(self):
        return self.name + " " + self.city

    def __str__(self):
        return self.name + " " + self.city

class Employer(models.Model):
    name = models.CharField(max_length=50, db_column="imie_pracownika")
    lastName = models.CharField(max_length=50, db_column="nazwisko_pracownika")
    telNumber = models.CharField(max_length=9, null=True, db_column="numer_telefonu")
    dateOfBirth = models.DateField(blank=True, null=True, db_column="rok_urodzenia")
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE, db_column="hotel")

    @property
    def Name(self):
        return self.name + " " + self.lastName

    def __str__(self):
        return self.name + " " + self.lastName

class Client(models.Model):
    name = models.CharField(max_length=45, db_column="imie")
    lastName = models.CharField(max_length=45, db_column="nazwisko")
    idNumber = models.CharField(max_length=10, null=True, db_column="numer dowodu osobistego")
    dateOfBirth = models.DateField(blank=True, null=True, db_column="rok_urodzenia")
    telNumber = models.CharField(max_length=9, null=True, db_column="numer_telefonu")

    @property
    def Name(self):
        return self.name + " " + self.lastName

    def __str__(self):
        return self.name + " " + self.lastName

class Service(models.Model):
    serviceName = models.CharField(max_length=100, db_column="nazwa_uslugi")
    serviceDescription = models.CharField(max_length=250, blank=True, db_column="opis_uslugi")

    def __str__(self):
        return self.serviceName

class ReservationPeriod(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, db_column="klient")
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE, db_column="hotel")
    startDate = models.DateField(default=now(), validators=[validate_startDate], db_column="od_kiedy")
    endDate = models.DateField(blank=False, null=True, validators=[validate_startDate], db_column="do_kiedy")
    roomNumber = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(200)],
                                             db_column="nr_pokoju", blank=False)

    def __str__(self):
        return self.client.Name+" "+str(self.id)

    def clean(self, *args, **kwargs):
        try:
            if ReservationPeriod.objects.filter(hotel=self.hotel, roomNumber=self.roomNumber):
                raise ValidationError("Ten Pokój jest już zajęty")
        except ValidationError:
            print("Oops!  That was no valid.  Try again...")
        super(ReservationPeriod, self).clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super(ReservationPeriod, self).save(*args, **kwargs)

class ServicePeriod(models.Model):
    reservation = models.ForeignKey(ReservationPeriod, null=True, on_delete=models.CASCADE, db_column="klient")
    serviceName = models.ForeignKey(Service, on_delete=models.CASCADE, db_column="usluga")
    employer = models.ForeignKey(Employer, db_column="pracownik")
    startDate = models.DateField(blank=False, null=True, default=now(), validators=[validate_startDate],
                                 db_column="od_kiedy")
    endDate = models.DateField(blank=False, null=True, validators=[validate_startDate], db_column="do_kiedy")
    addInformation = models.CharField(max_length=250, blank=True, db_column="dodatkowe_informacje")

    def __str__(self):
        return str(self.reservation_id) + " " + self.serviceName.serviceName

    def clean(self, *args, **kwargs):
        try:
            if not self.employer.hotel_id == self.reservation.hotel_id:
                raise ValidationError("Ten pracownik pracuje w innym hotelu")
        except ValidationError:
            print("Oops!  That was no valid.  Try again...")
        super(ServicePeriod, self).clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super(ServicePeriod, self).save(*args, **kwargs)
