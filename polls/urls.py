from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    url(r'^$',views.homepage, name='homepage'),
    url(r'^signup/$',views.registerNewEmployer, name='signup'),
    url(r'^login/$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^new_client/$', views.registerNewClient, name='registerNewClient'),
    url(r'^list_all_clients/$', views.listAllClients, name='listAllClientsFromEmployerHotel'),
    url(r'^list_my_services/$', views.listAllServicesAssignedToEmployer, name='listAllServiceAssignedToEmployer'),
    url(r'^list_all_services/active/$', views.listAllActiveServices, name='listAllActiveServices'),
    url(r'^list_all_services/history/$', views.listAllServicesHistory, name='listAllServicesHistory'),
    url(r'^list_all_reservations/active/$', views.listAllActiveReservations, name='listAllActiveReservations'),
    url(r'^list_all_reservations/history/$', views.listAllReservationsHistory, name='listAllReservationsHistory'),
    url(r'^client/([0-9]+)/$', views.listAllServicesAndReservationsForSpecificClient,
        name='listAllServicesAndReservationsForSpecificClient'),
    url(r'^client/([0-9]+)/new_service/$', views.registerNewServicePeriod, name='registerNewServicePeriod'),
    url(r'^client/([0-9]+)/new_reservation/$', views.registerNewReservationPeriod, name='registerNewReservationPeriod'),
    url(r'^client/([0-9]+)/edit_reservation/([0-9]*)/$', views.editReservationPeriod, name='editReservationPeriod'),
    url(r'^client/([0-9]+)/edit_service/([0-9]*)/$', views.editServicePeriod, name='editServicePeriod'),
    url(r'^profile/$', views.employerProfile, name='employerProfile'),
    url(r'^database/$', views.createFakeDatabase, name='createFakeDatabase'),
]