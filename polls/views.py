import random
import string
from datetime import timedelta

from django.contrib.auth.models import User
from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from polls.forms import SignUpEmployerForm, ClientForm, ServicePeriodForm,\
    ReservationPeriodForm
from django.template import loader
from django.utils.timezone import now
import sys

# Create your views here.
from polls.models import Client, Service, ServicePeriod, ReservationPeriod, Hotel, Employer


def homepage(request):
    return render(request, 'homepage.html')

def registerNewEmployer(request):
    if request.method == 'POST':
        form = SignUpEmployerForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
        else:
            return render(request, 'signup.html', {'form': form})
    else:
        form = SignUpEmployerForm()
    return render(request, 'signup.html', {'form': form})

@login_required
def registerNewClient(request):
    if request.method == 'POST':
        form = ClientForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
        else:
            return render(request, 'registerNewClient.html', {'form': form})
    else:
        form = ClientForm()
    return render(request, 'registerNewClient.html', {'form': form})

@login_required
def registerNewServicePeriod(request, clientId):
    if request.method == 'POST':
        form = ServicePeriodForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/client/' + clientId)
        else:
            return render(request, 'registerNewServicePeriod.html', {'form': form})
    else:
        reservation = ReservationPeriod.objects.filter(client_id=clientId)
        activeReservation = reservation.filter(endDate__gte=now())
        service = ServicePeriod(reservation=reservation.first())
        form = ServicePeriodForm(instance=service)
        form.fields['reservation'].queryset = activeReservation
        form.fields['employer'].queryset = Employer.objects.filter(hotel=activeReservation.first().hotel)
    return render(request, 'registerNewServicePeriod.html', {'form': form})

@login_required
def registerNewReservationPeriod(request, clientId):
    if request.method == 'POST':
        form = ReservationPeriodForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/client/' + clientId)
        else:
            return render(request, 'registerNewReservationPeriod.html', {'form': form})
    else:
        client = Client.objects.filter(id=clientId)
        reservation = ReservationPeriod(client=client.first())
        form = ReservationPeriodForm(instance=reservation)
        form.fields['client'].queryset = client
    return render(request, 'registerNewReservationPeriod.html', {'form': form})

@login_required
def editReservationPeriod(request, clientId, reservationId):
    if request.method == 'POST':
        form = ReservationPeriodForm(request.POST, instance=ReservationPeriod.objects.filter(id=reservationId).first())
        if form.is_valid():
            form.save()
            return redirect('/client/'+clientId)
    else:
        reservation = ReservationPeriod.objects.get(id=reservationId)
        form = ReservationPeriodForm(instance=reservation)
    return render(request, 'editReservationPeriod.html', {'form': form})

@login_required
def editServicePeriod(request, clientId, serviceId):
    if request.method == 'POST':
        form = ServicePeriodForm(request.POST, instance=ServicePeriod.objects.filter(id=serviceId).first())
        if form.is_valid():
            form.save()
            return redirect('/client/'+clientId)
    else:
        service = ServicePeriod.objects.get(id=serviceId)
        form = ServicePeriodForm(instance=service)
    return render(request, 'editReservationPeriod.html', {'form': form})

@login_required
def listAllClients(request):
    clients = Client.objects.order_by('-lastName').reverse()
    indexTemplate = loader.get_template('listAllClients.html')
    context = {
        'clients': clients,
    }
    return HttpResponse(indexTemplate.render(context, request))

@login_required
def listAllReservationsHistory(request):
    reservations = ReservationPeriod.objects.filter(endDate__lt=now())
    indexTemplate = loader.get_template('allReservationsHistory.html')
    context = {
        'reservations': reservations,
    }
    return HttpResponse(indexTemplate.render(context, request))

@login_required
def listAllServicesHistory(request):
    services = ServicePeriod.objects.filter(endDate__lt=now())
    indexTemplate = loader.get_template('allServicesHistory.html')
    context = {
        'services': services,
    }
    return HttpResponse(indexTemplate.render(context, request))

@login_required
def listAllActiveReservations(request):
    reservations = ReservationPeriod.objects.filter(endDate__gte=now())
    indexTemplate = loader.get_template('allActiveReservations.html')
    context = {
        'reservations': reservations,
    }
    return HttpResponse(indexTemplate.render(context, request))

@login_required
def listAllActiveServices(request):
    services = ServicePeriod.objects.filter(endDate__gte=now())
    print(services.__sizeof__())
    indexTemplate = loader.get_template('allActiveServices.html')
    context = {
        'services': services,
    }
    return HttpResponse(indexTemplate.render(context, request))

@login_required
def employerProfile(request):
    return render(request, 'employerProfile.html')

@login_required
def listAllServicesAssignedToEmployer(request):
    activeServices = ServicePeriod.objects.filter(employer=request.user.employer, endDate__gte=now()).order_by('-startDate')
    historyServices = ServicePeriod.objects.filter(
        employer=request.user.employer, endDate__lt=now()).order_by('-startDate')
    indexTemplate = loader.get_template('listServices.html')
    context = {
        'activeServices': activeServices,
        'historyServices': historyServices,
    }
    return HttpResponse(indexTemplate.render(context, request))

@login_required
def listAllServicesAndReservationsForSpecificClient(request, clientId):
    client = Client.objects.filter(id=clientId).first()
    activeServices = ServicePeriod.objects.filter(reservation__client_id=clientId, endDate__gte=now()).order_by('-startDate')
    historyServices = ServicePeriod.objects.filter(reservation__client_id=clientId, endDate__lt=now()).order_by('-startDate')
    activeReservations = ReservationPeriod.objects.filter(client_id=clientId, endDate__gte=now())
    historyReservations = ReservationPeriod.objects.filter(client_id=clientId, endDate__lt=now())
    indexTemplate = loader.get_template('client.html')
    context = {
        'client': client,
        'activeServices': activeServices,
        'historyServices': historyServices,
        'activeReservations': activeReservations,
        'historyReservations': historyReservations
    }
    return HttpResponse(indexTemplate.render(context, request))


def createFakeDatabase(request):
    names = ['Wojtek', 'Monika', 'Pawel', 'Piotr', 'Weronika', 'Radoslaw', 'Mateusz', 'Krzysztof',
             'Adam', 'Klaudiusz', 'Klaudia', 'Maciej', 'Dawid', 'Jaroslaw', 'Szymon', 'Katarzyna',
             'Anna', 'Ela', 'Ewa']
    lastNames = ['Nowak', 'Kowalczyk', 'Wozniak', 'Mazur', 'Fiutka', 'Krawczyk', 'Kaczmarek',
                 'Dudek', 'Walczak', 'Adamczyk', 'Stepien', 'Piejko', 'Pater', 'Krol', 'Zajac', 'Wilk']
    services = ['Sprzatanie', 'Zmiana poscieli', 'All inclusive', 'Sniadania', 'Obiady', 'Kolacje',
                'Parkowanie', 'Prasowanie', 'Pranie', 'Barek']
    idNumbers = [id_generator() for _ in range(40)]
    telNumbers = [telNumber_generator() for _ in range(40)]
    hotelNames = ['Hilton', 'Merkury']
    numberOfStars = [1, 2, 3, 4, 5]
    numberOfRooms = [250, 300, 325, 350]
    cities = ["Wroclaw", "Warszawa", "Krakow", "Berlin", "Monachium"]
    # generate Hotels
    for i in range(0,5):
        hotel = Hotel(city=cities[i], name=random.choice(hotelNames), numberOfStars=random.choice(numberOfStars),
                      numberOfRooms=random.choice(numberOfRooms))
        hotel.save()
    # generate clients
    for i in range (0,100):
        client = Client(name=random.choice(names), lastName=random.choice(lastNames), idNumber=random.choice(idNumbers),
                        dateOfBirth=now()-timedelta(days=random.randint(20*365, 70*365)), telNumber=random.choice(telNumbers))
        client.save()
    # generate employers
    for i in range(0, 30):
        name = random.choice(names)
        lastName = random.choice(lastNames)
        dateOfBirth = now()-timedelta(days=random.randint(20*365, 70*365))
        username = name[:3].lower()+lastName[:3].lower()+str(dateOfBirth.year)
        user = User.objects.create_user(username=username, password=username, email=username+'@gmail.com', first_name=name,
                    last_name=lastName)
        employer = Employer(name=name, lastName=lastName, telNumber=random.choice(telNumbers),
                            dateOfBirth=dateOfBirth, user=user,
                            hotel=Hotel.objects.filter(id=random.randint(1,5)).first())
        employer.save()
    # generate services
    for i in range(0,10):
        service = Service(serviceName=services[i], serviceDescription="")
        service.save()
    # generate reservations
    for i in range(0,70):
        reservation = ReservationPeriod(client=Client.objects.filter(id=i+1).first(),
                                        hotel=Hotel.objects.filter(id=random.randint(1,5)).first(), startDate=now(),
                                        endDate=now()+timedelta(days=10), roomNumber=random.randint(1,200))
        reservation.save()
    # generate servicePeriods
    for i in range(0,50):
        reservation = ReservationPeriod.objects.filter(id=random.randint(1,65)).first()
        servicePeriod = ServicePeriod(reservation=reservation,
                                      serviceName=Service.objects.filter(id=random.randint(1,9)).first(),
                                      employer=Employer.objects.filter(id=random.randint(1,28), hotel=reservation.hotel).first(),
                                      startDate=now(), endDate=now()+timedelta(days=8), addInformation="")
        servicePeriod.save()
    return render(request, 'homepage.html')

def id_generator(size=9, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def telNumber_generator(size=9, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))